from django.db import models

# Create your models here.
class homeTable(models.Model):
    id = models.AutoField(primary_key=True, null=False)
    name=models.CharField(max_length=100)
    image=models.ImageField(upload_to='images/', null=True, blank=True)
    value1=models.IntegerField()
    value2=models.IntegerField()
    # created_at = models.DateTimeField(auto_now_add=True)
    # updated_at = models.DateTimeField(auto_now=True)
    
    class Meta: 
         db_table ='homeTable'

    def __str__(self):
        return self.name
     